package com.springboot.bpm.urule.constant;

/**
 * @description 常量类
 * @author liuc
 * @date 2021/8/24 14:58
 * @since JDK1.8
 * @version V1.0
 */
public class Constant {
    /**
     * 逗号
     */
    public static final String COMMA = ",";
    /**
     * 句号
     */
    public static final String PERIOD = ".";
    /**
     * 竖杠
     */
    public static final String VERTICAL_BAR = "|";
    /**
     * 空字符串
     */
    public static final String NULLSTR = "";
    /**
     * 分号
     */
    public static final char SEMICOLON = ';';
    /**
     * 下划线
     */
    public static final String UNDERLINE = "_";
    /**
     * 等于号
     */
    public static final String EQUAL = "=";

    public static final String IS_NUMERIC = "[0-9]*";

    /**
     * 列表默认页码: 1
     */
    public static final int COMMON_PAGE_NUM = 1;

    /**
     * 列表默认显示数: 10
     */
    public static final int COMMON_PAGE_SIZE = 10;
}
