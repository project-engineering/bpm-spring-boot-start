package com.springboot.bpm.urule;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication(scanBasePackages = {"com.springboot.bpm"},exclude = {
		org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class,
		DataSourceAutoConfiguration.class
})
@MapperScan({"com.springboot.bpm.*.mapper"})
@ImportResource("classpath:urule-console-context.xml")
public class BpmUruleServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BpmUruleServerApplication.class, args);
	}

}
