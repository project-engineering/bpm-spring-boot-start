package com.springboot.bpm.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import jakarta.servlet.Filter;
import jakarta.servlet.Servlet;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * druid配置类
 * @author liuc
 * @version V1.0
 * @date 2021/11/9 14:51
 * @since JDK1.8
 */
@Log4j2
@Configuration
public class DruidConfig {

    @Value("${spring.datasource.druid.url}")
    private String connectionUrl;
    @Value("${spring.datasource.druid.username}")
    private String username;
    @Value("${spring.datasource.druid.password}")
    private String password;
    @Value("${spring.datasource.druid.driverClassName}")
    private String driverClassName;
    @Value("${spring.datasource.druid.initialSize}")
    private Integer initialSize;
    @Value("${spring.datasource.druid.minIdle}")
    private Integer minIdle;
    @Value("${spring.datasource.druid.maxActive}")
    private Integer maxActive;
    @Value("${spring.datasource.druid.maxWait}")
    private Integer maxWait;
    @Value("${spring.datasource.druid.timeBetweenEvictionRunsMillis}")
    private Integer timeBetweenEvictionRunsMillis;
    @Value("${spring.datasource.druid.minEvictableIdleTimeMillis}")
    private Integer minEvictableIdleTimeMillis;
    @Value("${spring.datasource.druid.validationQuery}")
    private String validationQuery;
    @Value("${spring.datasource.druid.testWhileIdle}")
    private Boolean testWhileIdle;
    @Value("${spring.datasource.druid.testOnBorrow}")
    private Boolean testOnBorrow;
    @Value("${spring.datasource.druid.testOnReturn}")
    private Boolean testOnReturn;
    @Value("${spring.datasource.druid.poolPreparedStatements}")
    private Boolean poolPreparedStatements;
    @Value("${spring.datasource.druid.maxOpenPreparedStatements}")
    private Integer maxOpenPreparedStatements;
    @Value("${spring.datasource.druid.maxPoolPreparedStatementPerConnectionSize}")
    private Integer maxPoolPreparedStatementPerConnectionSize;
    @Value("${spring.datasource.druid.filters}")
    private String filters;
    @Value("${spring.datasource.druid.loginUsername}")
    private String loginUsername;
    @Value("${spring.datasource.druid.loginPassword}")
    private String loginPassword;

    /**
     * 将自定义的 Druid数据源添加到容器中，不再让 Spring Boot 自动创建
     * 绑定全局配置文件中的 druid 数据源属性到 com.alibaba.druid.pool.DruidDataSource从而让它们生效
     * @ConfigurationProperties(prefix = "spring.datasource.druid")：作用就是将 全局配置文件中
     * 前缀为 spring.datasource.druid的属性值注入到 com.alibaba.druid.pool.DruidDataSource 的同名参数中
     * @param
     * @return javax.sql.DataSource
     * @author liuc
     * @date 2021/11/9 14:52
     * @throws
     */
    @Bean(name = "dataSource", initMethod = "init", destroyMethod = "close")
    public DruidDataSource dataSource() {
        log.info("初始化DruidDataSource");
        DruidDataSource dds = new DruidDataSource();
        dds.setUrl(connectionUrl);
        dds.setUsername(username);
        dds.setPassword(password);
        dds.setDriverClassName(driverClassName);
        dds.setInitialSize(initialSize);
        dds.setMinIdle(minIdle);
        dds.setMaxActive(maxActive);
        dds.setMaxWait(maxWait);
        dds.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        dds.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        dds.setValidationQuery(validationQuery);
        dds.setTestWhileIdle(testWhileIdle);
        dds.setTestOnBorrow(testOnBorrow);
        dds.setTestOnReturn(testOnReturn);
        dds.setPoolPreparedStatements(poolPreparedStatements);
        dds.setMaxOpenPreparedStatements(maxOpenPreparedStatements);
        dds.setMaxPoolPreparedStatementPerConnectionSize(maxPoolPreparedStatementPerConnectionSize);
        try {
            dds.setFilters(filters);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("初始化DruidDataSource异常,{}",e.getMessage());
        }
        log.info("初始化DruidDataSource完成");
        return dds;
    }

//    /**
//     * 配置Druid的监控
//     * 配置一个管理后台的Servlet
//     * @param
//     * @return org.springframework.boot.web.servlet.ServletRegistrationBean
//     * @author liuc
//     * @date 2021/11/9 14:55
//     * @throws
//     */
//    @Bean
//    public ServletRegistrationBean<StatViewServlet> druidServlet() {
//        ServletRegistrationBean<StatViewServlet> servletRegistrationBean = new ServletRegistrationBean<StatViewServlet>();
//        servletRegistrationBean.setServlet(new StatViewServlet());
//        //后台需要有人登录，进行配置
//        servletRegistrationBean.addUrlMappings("/druid/*");
//        Map<String, String> initParameters = new HashMap<String, String>(8);
//        // 用户名
//        initParameters.put("loginUsername", "admin");
//        // 密码
//        initParameters.put("loginPassword", "admin");
//        // 禁用HTML页面上的“Reset All”功能
//        initParameters.put("resetEnable", "false");
//        // IP白名单 (没有配置或者为空，则允许所有访问)
//        initParameters.put("allow", "");
//        // IP黑名单
//        /*initParameters.put("deny", "192.168.20.38");*/
//        initParameters.put("logSlowSql", "true");
//        servletRegistrationBean.setInitParameters(initParameters);
//        return servletRegistrationBean;
//    }
//
//    /**
//     * 配置一个web监控的filter
//     * 配置 Druid监控之web监控的filter
//     * WebStatFilter：用于配置Web和Druid数据源之间的管理关联监控统计
//     * @param
//     * @return FilterRegistrationBean
//     * @author liuc
//     * @date 2021/11/9 14:53
//     * @throws
//     */
//    @Bean
//    public FilterRegistrationBean webStatFilter(){
//        FilterRegistrationBean bean = new FilterRegistrationBean();
//        bean.setFilter((Filter) new WebStatFilter());
//        //exclusions：设置哪些请求进行过滤排除掉，从而不进行统计
//        Map<String,String> initParams = new HashMap<String,String>(8);
//        initParams.put("exclusions","*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
//        bean.setInitParameters(initParams);
//        //"/*" 表示过滤所有请求
//        bean.setUrlPatterns(Arrays.asList("/*"));
//        return bean;
//    }

}
