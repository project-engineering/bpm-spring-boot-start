package com.springboot.bpm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(scanBasePackages = {"com.springboot.bpm"},exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class,
        org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration.class,
        DataSourceAutoConfiguration.class
})
@MapperScan({"com.springboot.bpm.*.mapper"})
public class BpmWorkflowActivitiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BpmWorkflowActivitiApplication.class, args);
    }

}
