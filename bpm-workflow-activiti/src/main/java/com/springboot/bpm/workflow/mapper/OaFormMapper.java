package com.springboot.bpm.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.bpm.workflow.model.OaForm;

public interface OaFormMapper extends BaseMapper<OaForm> {

}