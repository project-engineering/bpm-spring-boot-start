package com.springboot.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.bpm.workflow.model.OaForm;

public interface OaFormService extends IService<OaForm> {

}
