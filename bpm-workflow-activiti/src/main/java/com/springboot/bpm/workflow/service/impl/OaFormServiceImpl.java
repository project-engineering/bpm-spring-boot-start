package com.springboot.bpm.workflow.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.bpm.workflow.mapper.OaFormMapper;
import com.springboot.bpm.workflow.model.OaForm;
import com.springboot.bpm.workflow.service.OaFormService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class OaFormServiceImpl extends ServiceImpl<OaFormMapper, OaForm> implements OaFormService {


}
