package com.springboot.bpm.workflow.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.springboot.bpm.workflow.common.vo.Resp;
import com.springboot.bpm.workflow.common.vo.enums.CodeMsgEnum;
import com.springboot.bpm.workflow.components.SnowflakeComponent;
import com.springboot.bpm.workflow.exception.BizException;
import com.springboot.bpm.workflow.model.OaForm;
import com.springboot.bpm.workflow.param.OaFormParam;
import com.springboot.bpm.workflow.service.OaFormService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@RequestMapping("/designer/form")
@RestController
@Log4j2
public class DesignerFormController {
    @Autowired
    private OaFormService oaFormService;

    @Autowired
    private SnowflakeComponent snowflake;

    /**
     * 表单生成器首页
     * @return
     */
    @GetMapping("/index")
    public ModelAndView index() {
        return new ModelAndView("redirect:/designerForm.html");
    }

    /**
     * 自定义表单分页查询
     * @param request
     * @return
     */
    @GetMapping("/page")
    public Resp<Page> page(HttpServletRequest request){
        Page<OaForm> page = getOaFormPage(request);
        Wrapper<OaForm> query = getOaFormWrapper(request);
        Page<OaForm> data = oaFormService.page(page, query);
        return Resp.OK(data);

    }

    /**
     * 自定义表单新增
     * @param param
     * @return
     */
    @PostMapping("/insert")
    public Resp<String> insert(@RequestBody OaFormParam param){
        OaForm entity = new OaForm();
        BeanUtil.copyProperties(param,entity);
        if (StringUtils.isEmpty(entity.getId())) {
            entity.setId(String.valueOf(snowflake.snowflakeId()));
        }
        boolean flag = oaFormService.save(entity);
        if (flag) {
            return  Resp.OK_WITHOUT_DATA();
        }
        return Resp.ERROR(CodeMsgEnum.ERROR);

    }

    @GetMapping("/delete/{id}")
    public Resp delete(@PathVariable String id){
        if (StringUtils.isEmpty(id)) {
            throw new BizException(CodeMsgEnum.NO_MATCH);
        }
        boolean flag = oaFormService.removeById(id);
        if (flag) {
            return Resp.OK_WITHOUT_DATA();
        }
        return Resp.ERROR(CodeMsgEnum.NO_MATCH);
    }

    @PostMapping(value ="/update", produces = "application/json;charset=utf-8")
    public Resp update(@RequestBody OaFormParam param){
        OaForm entity = new OaForm();
        BeanUtil.copyProperties(param,entity);
        if (StringUtils.isEmpty(entity.getId())) {
            throw new BizException(CodeMsgEnum.NO_MATCH);
        }
        boolean flag = oaFormService.updateById(entity);
        if (flag) {
            Resp.OK_WITHOUT_DATA();
        }
        return Resp.ERROR(CodeMsgEnum.ERROR);
    }

    private Wrapper<OaForm> getOaFormWrapper(HttpServletRequest request) {
        String name = request.getParameter("name");
        QueryWrapper<OaForm> query  = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(name)) {
            query.like("name", name);
        }
        return query;
    }

    private Page<OaForm> getOaFormPage(HttpServletRequest request) {
        String pageParame = request.getParameter("page");
        String sizeParame = request.getParameter("size");

        Integer page = StringUtils.isNotEmpty(pageParame) ? Integer.valueOf(pageParame) : 1;
        Integer size = StringUtils.isNotEmpty(sizeParame) ? Integer.valueOf(sizeParame) : 10;

        return new Page<>(page, size);
    }

}
