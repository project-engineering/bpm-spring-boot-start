package com.springboot.bpm.workflow.handler;

import com.springboot.bpm.workflow.common.vo.Resp;
import com.springboot.bpm.workflow.common.vo.enums.CodeMsgEnum;
import com.springboot.bpm.workflow.exception.BizException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 全局异常捕获处理
 */
@ControllerAdvice
@Log4j2
public class GlobalExceptionHandler {

    /**
     * 处理自定义的业务异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = BizException.class)
    @ResponseBody
    public Resp bizExceptionHandler(HttpServletRequest req, BizException e){
        log.error("发生业务异常！原因是：{}",e.getMessage());
        return Resp.ERROR(CodeMsgEnum.NO_MATCH);
    }

    /**
     * 处理其他异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value =Exception.class)
    @ResponseBody
    public Resp exceptionHandler(HttpServletRequest req, Exception e){
        log.error("未知异常！原因是:",e);
        return Resp.ERROR(CodeMsgEnum.ERROR);
    }
}
